import torch
import torch.nn as nn

from torchvision import transforms
from torch.optim import Adam
from torch.nn.functional import cross_entropy
from torch.nn.functional import softmax
from torch.utils.data import DataLoader
from pytorch_lightning.metrics import Accuracy
from pytorch_lightning.core.lightning import LightningModule
from torchvision.models import resnet18
from torchvision.datasets import CIFAR10


class CIFARMultiGPUModule(LightningModule):

    def __init__(self):
        super(CIFARMultiGPUModule, self).__init__()
        self.model = resnet18(pretrained=True)
        in_features = self.model.fc.in_features
        self.model.fc = nn.Sequential(
            nn.Linear(in_features=in_features, out_features=10),
            nn.LogSoftmax(dim=1)
        )
        self.train_acc = Accuracy()
        self.test_acc = Accuracy()

    def forward(self, inputs):
        classifier = self.model(inputs)
        return classifier

    def training_step(self, batch, batch_idx):
        images, targets = batch
        preds = self(images)
        return {'pred': preds, 'target': targets}

    def training_step_end(self, outputs):
        preds = outputs['pred']
        targets = outputs['target']
        loss = cross_entropy(preds, targets)
        acc = self.train_acc(softmax(preds, 1), targets)
        metrics = {
            'train_loss': loss,
            'train_acc': acc
        }
        self.log_dict(metrics, prog_bar=True)
        return {'loss': loss, 'train_loss': loss}

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([output['loss'] for output in outputs]).mean()
        metrics = {
            'train_loss_epoch': avg_loss,
            'train_acc_epoch': self.train_acc.compute()
        }
        self.log_dict(metrics)

    def test_step(self, batch, batch_idx):
        images, targets = batch
        outputs = self(images)
        return {'pred': outputs, 'target': targets}

    def test_step_end(self, outputs):
        preds = outputs['pred']
        targets = outputs['target']
        loss = cross_entropy(preds, targets)
        acc = self.test_acc(softmax(preds, 1), targets)
        metrics = {
            'test_loss': loss,
            'test_acc': acc
        }
        self.log_dict(metrics, prog_bar=True)
        return {'loss': loss, 'test_loss': loss}

    def test_epoch_end(self, outputs):
        avg_loss = torch.stack([output['loss'] for output in outputs]).mean()
        metrics = {
            'test_loss_epoch': avg_loss,
            'test_acc_epoch': self.test_acc.compute()
        }
        self.log_dict(metrics)

    def configure_optimizers(self):
        optimizer = Adam(self.parameters(), lr=0.0001)
        return optimizer

    def train_dataloader(self):
        transform = transforms.Compose([
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        ])
        dataset = CIFAR10(root='./data', train=False, download=True, transform=transform)
        loader = DataLoader(
            dataset=dataset,
            batch_size=64*torch.cuda.device_count(),
            shuffle=True, num_workers=4
        )
        return loader

    def test_dataloader(self):
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            )
        ])
        dataset = CIFAR10(root='./data', train=False, download=True, transform=transform)
        loader = DataLoader(
            dataset=dataset,
            batch_size=64*torch.cuda.device_count(),
            shuffle=False,
            num_workers=4
        )
        return loader
