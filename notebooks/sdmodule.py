import argparse
import torch
import torch.nn as nn

from torchvision import transforms
from torch.optim import Adam
from torch.nn.functional import cross_entropy
from torch.nn.functional import softmax
from torch.utils.data import DataLoader
from pytorch_lightning.metrics import Accuracy
from pytorch_lightning.core.lightning import LightningModule
from torchvision.models import resnet18
from torchvision.datasets import CIFAR10


class CIFARSingleDeviceModule(LightningModule):

    def __init__(self):
        super(CIFARSingleDeviceModule, self).__init__()
        self.model = resnet18(pretrained=True)
        in_features = self.model.fc.in_features
        self.model.fc = nn.Sequential(
            nn.Linear(in_features=in_features, out_features=10),
            nn.LogSoftmax(dim=1)
        )
        self.train_acc = Accuracy()
        self.val_acc = Accuracy()

    def forward(self, inputs):
        classifier = self.model(inputs)
        return classifier

    def training_step(self, batch, batch_idx):
        images, labels = batch
        outputs = self(images)
        loss = cross_entropy(outputs, labels)
        acc = self.train_acc(softmax(outputs, 1), labels)
        metrics = {
            'train_loss': loss,
            'train_acc': acc
        }
        self.log_dict(metrics, prog_bar=True)
        return {'loss': loss, 'train_loss': loss}

    def training_epoch_end(self, outputs):
        avg_loss = torch.stack([output['train_loss'] for output in outputs]).mean()
        metrics = {
            'train_loss_epoch': avg_loss,
            'train_acc_epoch': self.train_acc.compute()
        }
        self.log_dict(metrics)

    def validation_step(self, batch, batch_idx):
        images, labels = batch
        outputs = self(images)
        loss = cross_entropy(outputs, labels)
        acc = self.val_acc(softmax(outputs, 1), labels)
        metrics = {
            'val_loss': loss,
            'val_acc': acc
        }
        self.log_dict(metrics)
        return {'loss': loss}

    def validation_epoch_end(self, outputs):
        avg_loss = torch.stack([output['loss'] for output in outputs]).mean()
        metrics = {
            'val_loss_epoch': avg_loss,
            'val_acc_epoch': self.val_acc.compute()
        }
        self.log_dict(metrics)

    def configure_optimizers(self):
        optimizer = Adam(self.parameters(), lr=0.0001)
        return optimizer

    def train_dataloader(self):
        transform = transforms.Compose([
            transforms.RandomHorizontalFlip(),
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            ),
        ])
        dataset = CIFAR10(root='./data', train=True, download=True, transform=transform)
        loader = DataLoader(dataset=dataset, batch_size=64, shuffle=True, num_workers=4, pin_memory=True)
        return loader

    def val_dataloader(self):
        transform = transforms.Compose([
            transforms.ToTensor(),
            transforms.Normalize(
                mean=[0.485, 0.456, 0.406],
                std=[0.229, 0.224, 0.225]
            ),
        ])
        dataset = CIFAR10(root='./data', train=False, download=True, transform=transform)
        loader = DataLoader(dataset=dataset, batch_size=64, shuffle=False, num_workers=4, pin_memory=True)
        return loader
