from mgpumodule import CIFARMultiGPUModule
from pytorch_lightning import loggers
from pytorch_lightning import Trainer
from pytorch_lightning.callbacks import ModelCheckpoint


if __name__ == '__main__':
    checkpoint = ModelCheckpoint(
        monitor='val_acc_epoch',
        mode='max',
        verbose=True,
        filename='cifar-{epoch:03d}'
    )
    board_logger = loggers.TensorBoardLogger(
        save_dir='./metrics'
    )
    trainer = Trainer(
        max_epochs=10,
        gpus=2,
        callbacks=[checkpoint],
        logger=[board_logger],
        default_root_dir='./ckpts',
        accelerator='ddp'
    )
    model = CIFARMultiGPUModule()
    trainer.fit(model=model)
    trainer.test(model=model)
